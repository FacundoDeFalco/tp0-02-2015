package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.Optional;
import java.util.Stack;

public class RPNCalculator {
    private void checkValidExpression(String expression) {
        Optional.ofNullable(expression).orElseThrow(() -> new IllegalArgumentException("Invalid expression."));
    }

    private void checkNoNullOperator(Operator operator) {
        Optional.ofNullable(operator).orElseThrow(() -> new IllegalArgumentException("Invalid operator."));
    }

    private boolean isBinaryOperator(String sign) {
        int length = sign.length();
        if ((length % 2) == 0) {
            int middle = length / 2;
            String string1 = sign.substring(0, middle);
            String string2 = sign.substring(middle);
            return (string1.compareTo(string2) != 0);
        }
        return true;
    }

    private void checkParametersAmount(Stack stack, int minAmount) {
        Optional.of(stack).filter((stackAux) -> (stackAux.size() >= minAmount))
                .orElseThrow(() -> new IllegalArgumentException("Incorrect quantity of operands."));
    }

    private void calculate(Stack stack, Operator operator, String sign) {
        int minimum;
        int begin;
        if (isBinaryOperator(sign)) {
            minimum = operator.MIN_BINARY;
            begin = stack.size() - 1;
        } else {
            minimum = operator.MIN_N_ARY;
            begin = 1;
        }
        checkParametersAmount(stack, minimum);
        float result = Float.parseFloat((String) stack.get(begin - 1));
        float operand;
        for (int i = begin; i < stack.size(); i++) {
            operand = Float.parseFloat((String) stack.get(i));
            result = operator.operation(result, operand);
            stack.setElementAt(String.valueOf(result), i - 1);
            stack.remove(i);
            i--;
        }
    }

    private HashMap createMap() {
        HashMap<String, Operator> map = new HashMap<String, Operator>();
        Operator add = (float number1, float number2) -> number1 + number2;
        Operator sub = (float number1, float number2) -> number1 - number2;
        Operator mul = (float number1, float number2) -> number1 * number2;
        Operator div = (float number1, float number2) -> number1 / number2;
        Operator mod = (float number1, float number2) -> number1 % number2;
        map.put("+", add);
        map.put("-", sub);
        map.put("*", mul);
        map.put("/", div);
        map.put("MOD", mod);
        map.put("++", add);
        map.put("--", sub);
        map.put("**", mul);
        map.put("//", div);
        return map;
    }

    private boolean isNumber(String string) {
        return string.matches("^[-+]?\\d+\\.?\\d*$");
    }

    private void resolveValue(Stack stack, HashMap operatorsMap, String value) {
        if (isNumber(value)) {
            stack.push(value);
        } else {
            Operator operator = (Operator) operatorsMap.get(value);
            checkNoNullOperator(operator);
            calculate(stack, operator, value);
        }
    }

    public float eval(String expression) {
        checkValidExpression(expression);
        String[] values = expression.split(" ");
        Stack stack = new Stack();
        HashMap operatorsMap = createMap();
        for (int i = 0; i < values.length; i++) {
            resolveValue(stack, operatorsMap, values[i]);
        }
        return Float.parseFloat((String) stack.pop());
    }
}