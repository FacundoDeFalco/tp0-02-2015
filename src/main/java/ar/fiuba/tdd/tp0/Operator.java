package ar.fiuba.tdd.tp0;

public interface Operator {
    final int MIN_BINARY = 2;
    final int MIN_N_ARY = 1;

    float operation(float result, float operand);
}
